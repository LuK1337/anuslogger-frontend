<?php
/**
 * Created by PhpStorm.
 * User: luk
 * Date: 7/26/15
 * Time: 9:43 PM
 */

class Logger {
    private static $pdo;

    function connect() {
        $this->pdo = new PDO('sqlite:logs.db');
    }

    function getDate() {
        if (isset($_GET['day']) && (int)$_GET['day'] > 0) {
            $start = strtotime("midnight", (int)$_GET['day']);
        } else {
            $start = strtotime("midnight");
        }

        return array($start, strtotime("tomorrow", $start) - 1);
    }

    function getMessages() {
        $last_id = (isset($_GET['last_id']) && (int) $_GET['last_id'] > 0) ? (int) $_GET['last_id'] : 0;
        $sql = sprintf('SELECT * from logs WHERE timestamp > %d AND timestamp < %d AND id > %d', $this->getDate()[0], $this->getDate()[1], $last_id);
        $query = $this->pdo->query($sql);
        $query->execute();

        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}