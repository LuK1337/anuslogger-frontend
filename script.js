/**
 * Created by LuK on 2015-07-27.
 */

var last_id = 0;
var day = -1;
var autoupdate = false;
var last_nick = "";

function update() {
    $.getJSON("api.php?last_id=" + String(last_id) + "&day=" + String(day), function(data) {
        $.each(data, function(key, val) {
            var date = new Date(val["timestamp"] * 1000);
            var time = date.toISOString().match(/(\d{4}\-\d{2}\-\d{2})T(\d{2}:\d{2}:\d{2})/)
            var curr_nick = last_nick == escapeHtml(String(val["nick"])) ? "" : escapeHtml(String(val["nick"]));
            last_nick = escapeHtml(String(val["nick"]));

            if (val["action"] == "join") {
                curr_nick = escapeHtml(String(val["nick"]));
                
                message = "<li id='" + String(val["id"]) + "'>" +
                "<div id='nick' class='join'>#titandev</div>" +
                "<div id='message'>" + curr_nick + " joined</div>" +
                "<div id='time'>" + time[2] + "</div></li>";
            } else if (val["action"] == "quit") {
                curr_nick = escapeHtml(String(val["nick"]));

                message = "<li id='" + String(val["id"]) + "'>" +
                "<div id='nick' class='quit'>#titandev</div>" +
                "<div id='message'>" + curr_nick + " left</div>" +
                "<div id='time'>" + time[2] + "</div></li>";
            } else if (val["action"] == "kick") {
                curr_nick = escapeHtml(String(val["nick"]));

                message = "<li id='" + String(val["id"]) + "'>" +
                "<div id='nick' class='kick'>#titandev</div>" +
                "<div id='message'>" + curr_nick + " has been kicked</div>" +
                "<div id='time'>" + time[2] + "</div></li>";
            } else {
                message = "<li id='" + String(val["id"]) + "'>" +
                "<div id='nick'>" + curr_nick + "</div>" +
                "<div id='message'>" + escapeHtml(String(val["message"])) + "</div>" +
                "<div id='time'>" + time[2] + "</div></li>";
            }

            $("#messages").append(message);
        });

        last_id = data[data.length - 1]["id"];

        if (autoupdate) {
            $('html, body').animate({
                scrollTop: $("#" + String(last_id)).offset().top
            }, 2000);
        }
    });
}

function escapeHtml(text) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };

    return linkify(text.replace(/[&<>"']/g, function(m) { return map[m]; }));
}

function linkify(inputText) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;

    //URLs starting with http://, https://, or ftp://
    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

    //Change email addresses to mailto:: links.
    replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

    return replacedText;
}

update();
setInterval(update, 5000);

// set current date
var date = new Date();
var datetime = date.toISOString().match(/(\d{4}\-\d{2}\-\d{2})T(\d{2}:\d{2}:\d{2})/);
$("#datetime").html(datetime[1]);

$("#autoscroll").click(function() {
    autoupdate = !autoupdate;
    attr = autoupdate ? "btn-success" : "btn-danger";

    $(this).attr("class", "btn " + attr);

    if (autoupdate) {
        $('html, body').animate({
            scrollTop: $("#" + String(last_id)).offset().top
        }, 2000);
    }
});

$('#date').datepicker().on('changeDate', function(ev) {
    console.log(ev);
    day = ev.date.valueOf() / 1000;

    var date = new Date(ev.date.valueOf() + 8640000);
    var datetime = date.toISOString().match(/(\d{4}\-\d{2}\-\d{2})T(\d{2}:\d{2}:\d{2})/);

    $("#datetime").html(datetime[1]);
    $("#messages").html("");

    last_id = -1;
    last_nick = "";
    autoupdate = false;
    $("#autoscroll").attr("class", "btn btn-danger");

    update();
});