<?php
/**
 * Created by PhpStorm.
 * User: luk
 * Date: 7/27/15
 * Time: 2:37 PM
 */

include "logger.php";

$log = new Logger();
$log->connect();

echo json_encode($log->getMessages());